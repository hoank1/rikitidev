
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Music is my life</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/sticky-footer-navbar.css" rel="stylesheet">
  </head>

  <body>

    <header>
      <!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Music</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
              <a class="nav-link disabled" href="#">Disabled</a>
            </li>
          </ul>
          <form class="form-inline mt-2 mt-md-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form>
        </div>
      </nav>
    </header>

    <!-- Begin page content -->
    <main role="main" class="container">
      <div class="row">
      	<div class="col-8 border">
      	<h1>We will come back soon</h1>
     	<div>
     		 <iframe width="730" height="360" src="https://www.youtube.com/embed/HXxXoLztcXw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
     	</div>
      </div>
      <div class="col-sm-1"></div>
      <div class="col-3 border">
      	<div>
      		<div class="btn-group btn-group-toggle" data-toggle="buttons">
			  <label class="btn btn-secondary active">
			    <input type="radio" name="options" id="option1" autocomplete="off" checked> Active
			  </label>
			  <label class="btn btn-secondary">
			    <input type="radio" name="options" id="option2" autocomplete="off"> Radio
			  </label>
			  <label class="btn btn-secondary">
			    <input type="radio" name="options" id="option3" autocomplete="off"> Radio
			  </label>
			</div>
      	</div>
      	<br>
      	<div>
      		<iframe width="250" height="200" src="https://www.youtube.com/embed/S8kKkdo9uaY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      	</div>
      	<div>
      		<iframe width="250" height="200" src="https://www.youtube.com/embed/UNCkrfKZ23o" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      	</div>
      	<div>
      		<iframe width="250" height="200" src="https://www.youtube.com/embed/JocuzX7DXkk?list=PLMWIQ-S7405-oiaslrvP5b-PEfTSH4SR8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      	</div>
      </div>
      </div>
    </main>
	<br>
    <footer class="footer">
      <div class="container">
        <span class="text-muted">Please waiting for us !!!</span>
        <span id="timer"></span>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="dist/js/jquery-3.3.1.min.js"></script>
    <!-- <script src="assets/js/popper.min.js"></script> -->
    <script src="dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
  </body>
</html>
